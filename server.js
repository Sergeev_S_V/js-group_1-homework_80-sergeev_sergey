const express = require('express');
const mysql = require('mysql');

const categories = require('./app/categories');
const locations = require('./app/locations');
const subjects = require('./app/subjects');

const app = express();

const port = 8000;

app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'lsn-79-homework'
});

connection.connect((err) => {
  if (err) throw err;

  app.use('/categories', categories(connection));
  app.use('/locations', locations(connection));
  app.use('/subjects', subjects(connection));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});



