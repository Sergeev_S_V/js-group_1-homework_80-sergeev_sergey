const express = require('express');
const router = express.Router();


const createRouter = (db) => {
  router.get('/', (req, res) => {
    db.query('SELECT `category`, `id` FROM `categories`', (err, result) => {
      if (err) throw err;
      res.send(result);
    })
  });

  router.get('/:id', (req, res) => {
    db.query(
      'SELECT * ' +
      'FROM `lsn-79-homework`.categories WHERE id = ' + req.params.id,
      (err, result) => {
        res.send(result);
      }
    );
  });

  router.post('/', (req, res) => {
    const category = req.body;

    if (category.category) {
      db.query(
        'INSERT INTO categories(`category`, `description`)' +
        'VALUES (?,?)',
        [category.category, category.description],
        (err, result) => {
          category.id = result.insertId;
          res.send(category);
        }
      );
    } else {
      res.send('Enter all field');
    }
  });

  router.delete('/:id', (req, res) => {
    db.query(
      'DELETE FROM `lsn-79-homework`.categories where id = ' + req.params.id,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          res.send('Category was deleted');
        }
      }
    );
  });

  router.put('/:id', (req, res) => {
    const category = req.body;
    let updateCategory = 'UPDATE `lsn-79-homework`.categories ' +
      'SET description = \'' + category.description +
      '\', category = \'' + category.category +
      '\' WHERE id = ' + req.params.id;

    db.query(
      updateCategory,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          category.id = req.params.id;
          res.send(category);
        }
      }
    );
  });

  return router;
};

module.exports = createRouter;