const express = require('express');
const router = express.Router();
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const createRouter = (db) => {
  router.get('/', (req, res) => {
    db.query(
      'SELECT `id`, `category_id`, `location_id`, `name` FROM `subjects`',
      (err, result) => {
      if (err) throw err;

      res.send(result);
    })
  });

  router.get('/:id', (req, res) => {
    db.query(
      'SELECT * ' +
      'FROM `lsn-79-homework`.subjects WHERE id = ' + req.params.id,
      (err, result) => {
        res.send(result);
      }
    );
  });

  router.post('/', upload.single('image'), (req, res) => {
    const subject = req.body;

    if (subject.category_id && subject.location_id && subject.name) {

      if (req.file) {
        subject.image = req.file.filename;
      } else {
        subject.image = null;
      }

      db.query(
        'INSERT INTO subjects (`name`, `category_id`, `location_id`, `description`, `image`)' +
        'VALUES (?,?,?,?,?)',
        [subject.name, subject.category_id, subject.location_id, subject.description, subject.image],
        (err, result) => {
          subject.id = result.insertId;
          res.send(subject);
        }
      );
    } else {
      res.send('Enter all field');
    }
  });

  router.delete('/:id', (req, res) => {
    db.query(
      'DELETE FROM `lsn-79-homework`.subjects where id = ' + req.params.id,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          res.send('Subject was deleted');
        }
      }
    );
  });

  router.put('/:id', (req, res) => {
    const subject = req.body;

    let updateSubject = 'UPDATE `lsn-79-homework`.subjects ' +
      'SET name = \'' + subject.name +
      '\', location_id = \'' + subject.location_id +
      '\', category_id = \'' + subject.category_id +
      '\', description = \'' + subject.description +
      '\', image = \'' + subject.image +
      '\' WHERE id = ' + req.params.id;
    db.query(
      updateSubject,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          subject.id = req.params.id;
          res.send(subject);
        }
      }
    );
  });

  return router;
};

module.exports = createRouter;


