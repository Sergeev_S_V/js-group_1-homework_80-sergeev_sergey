const express = require('express');
const router = express.Router();


const createRouter = (db) => {
  router.get('/', (req, res) => {
    db.query('SELECT `location`, `id` FROM `locations`', (err, result) => {
      if (err) throw err;

      res.send(result);
    })
  });

  router.get('/:id', (req, res) => {
    db.query(
      'SELECT * ' +
      'FROM `lsn-79-homework`.locations WHERE id = ' + req.params.id,
      (err, result) => {
        res.send(result);
      }
    );
  });

  router.post('/', (req, res) => {
    const location = req.body;

    if (location.location) {
      db.query(
        'INSERT INTO locations(`location`, `description`)' +
        'VALUES (?,?)',
        [location.location, location.description],
        (err, result) => {
          location.id = result.insertId;
          res.send(location);
        }
      );
    } else {
      res.send('Enter all field');
    }
  });

  router.delete('/:id', (req, res) => {
    db.query(
      'DELETE FROM `lsn-79-homework`.locations where id = ' + req.params.id,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          res.send('Location was deleted');
        }
      }
    );
  });

  router.put('/:id', (req, res) => {
    const location = req.body;
    let updateLocation = 'UPDATE `lsn-79-homework`.locations ' +
      'SET description = \'' + location.description +
    '\', location = \'' + location.location +
    '\' WHERE id = ' + req.params.id;

    db.query(
      updateLocation,
      (err) => {
        if (err) {
          res.send(err.sqlMessage);
        } else {
          location.id = req.params.id;
          res.send(location);
        }
      }
    );
  });

  return router;
};

module.exports = createRouter;